﻿namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Customer
{
    public class CreateCustomerRequest
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public List<short> PreferencesIds { get; set; }
    }
}
