﻿namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Empoloyee
{
    public class CreateEmployeeRequest
    {
        public required string LastName { get; set; }

        public required string FirstName { get; set; }

        public string? MiddleName { get; set; }

        public string? Email { get; set; }

        public short RoleId { get; set; }
    }
}
