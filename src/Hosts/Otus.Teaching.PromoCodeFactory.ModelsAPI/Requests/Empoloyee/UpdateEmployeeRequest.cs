﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Empoloyee
{
    public class UpdateEmployeeRequest
    {
        public string? LastName { get; set; }

        public string? FirstName { get; set; }

        public string? MiddleName { get; set; }

        public string? Email { get; set; }

        public short RoleId { get; set; }
    }
}
