﻿namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Preference
{
    public class PreferenceResponse
    {
        public short Id { get; set; }

        public string Name { get; set; }
    }
}
