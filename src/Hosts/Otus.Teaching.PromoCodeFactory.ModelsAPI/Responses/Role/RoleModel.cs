﻿using System;

namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Role
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}