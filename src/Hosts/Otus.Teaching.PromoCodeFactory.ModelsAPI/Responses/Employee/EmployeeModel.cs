﻿using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Role;

namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Employee
{
    public class EmployeeModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public RoleModel Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}