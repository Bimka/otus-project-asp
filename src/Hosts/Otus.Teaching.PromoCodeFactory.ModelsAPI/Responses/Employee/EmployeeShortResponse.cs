﻿using System;

namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Employee
{
    public class EmployeeShortResponse
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }
    }
}