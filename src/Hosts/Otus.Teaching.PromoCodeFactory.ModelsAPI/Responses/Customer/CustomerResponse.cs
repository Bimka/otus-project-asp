﻿using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Preference;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Customers
{
    public class CustomerResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromocodeShortResponse> PromoCodes { get; set; }
    }
}
