﻿namespace Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Customers
{
    public class CustomerShortResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
