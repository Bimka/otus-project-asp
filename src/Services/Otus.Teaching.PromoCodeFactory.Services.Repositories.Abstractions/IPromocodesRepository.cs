﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;

namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    public interface IPromocodesRepository : IRepository<Promocode, int>
    {
    }
}
