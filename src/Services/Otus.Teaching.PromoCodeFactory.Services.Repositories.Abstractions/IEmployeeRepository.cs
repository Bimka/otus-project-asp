﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;

namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    public interface IEmployeeRepository : IRepository<Employee, int>
    {
    }
}
