﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using System.Security.Cryptography.X509Certificates;

namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    public interface ICustomersRepository : IRepository<Customer, int>
    {
        public new Task Delete(int id);
    }
}
