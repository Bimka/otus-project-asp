﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;

namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    public interface IPreferencesRepository : IRepository<Preference, short>
    {
    }
}
