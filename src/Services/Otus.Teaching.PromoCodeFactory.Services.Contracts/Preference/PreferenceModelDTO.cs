﻿namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Preference
{
    public class PreferenceModelDTO
    {
        public short Id { get; set; }

        public required string Name { get; set; }
    }
}
