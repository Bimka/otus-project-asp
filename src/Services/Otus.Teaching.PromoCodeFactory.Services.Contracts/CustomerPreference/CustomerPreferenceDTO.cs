﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Preference;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.CustomerPreference
{
    public class CustomerPreferenceDTO
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public short PreferenceId { get; set; }

        public CustomerModelDTO Customer { get; set; }

        public PreferenceModelDTO Preference { get; set; }
    }
}
