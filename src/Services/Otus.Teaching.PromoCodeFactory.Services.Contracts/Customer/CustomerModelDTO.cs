﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.CustomerPreference;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer
{
    public class CustomerModelDTO
    {
        public int Id { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public ICollection<CustomerPreferenceDTO> CustomerPreferences { get; set; }
    }
}
