﻿namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer
{
    public class UpdateCustomerDTO
    {
        public int Id { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public List<short> PreferencesIds { get; set; }
    }
}
