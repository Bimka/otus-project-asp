﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.CustomerPreference;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer
{
    public class CreateCustomerDTO
    {
        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public List<short> PreferencesIds { get; set; }
    }
}
