﻿namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Employee
{
    public class UpdateEmployeeDTO
    {
        public required string LastName { get; set; }

        public required string FirstName { get; set; }

        public string? MiddleName { get; set; }

        public string? Email { get; set; }

        public short RoleId { get; set; }
    }
}
