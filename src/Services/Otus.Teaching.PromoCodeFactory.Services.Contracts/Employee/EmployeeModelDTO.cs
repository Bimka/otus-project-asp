﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Role;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Employee
{
    public class EmployeeModelDTO
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public RoleModelDTO Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
