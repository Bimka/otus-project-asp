﻿namespace Otus.Teaching.PromoCodeFactory.Services.Contracts.Promocode
{
    public class PromocodeModelDTO
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public short RoleId { get; set; }

        public short PreferenceId { get; set; }
    }
}
