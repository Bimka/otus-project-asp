﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer;
using Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class CustomersService(ICustomersRepository customersRepository) : ICustomersService
    {
        private readonly ICustomersRepository _customersRepository = customersRepository;
        private readonly CustomerMapper _customerMapper = new ();


        public async Task<int> CreateCustomerAsync(CreateCustomerDTO customer)
        {
            var customerResult = await _customersRepository.AddAsync(_customerMapper.ConvertCreateCustomerDtoToCustomer(customer));
            await _customersRepository.SaveChangesAsync();
            return customerResult.Id;
        }

        public async Task DeleteCustomerAsync(int id)
        {
            await _customersRepository.Delete(id);
            await _customersRepository.SaveChangesAsync();
        }


        public async Task EditCustomersAsync(UpdateCustomerDTO customer)
        {
            _customersRepository.ChangeTrackerClear();
            _customersRepository.Update(_customerMapper.ConvertUpdateCustomerDtoToCustomer(customer));
            await _customersRepository.SaveChangesAsync();
        }

        public async Task<List<CustomerModelDTO>> GetAllAsync()
        {
            return _customerMapper.ConvertCustomerToCustomerModelDto(await _customersRepository.GetAllAsync(CancellationToken.None));
        }

        public async Task<CustomerModelDTO> GetByIdAsync(int id)
        {
            return _customerMapper.ConvertCustomerToCustomerModelDto(await _customersRepository.GetAsync(id, CancellationToken.None));
        }
    }
}
