﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Employee;
using Riok.Mapperly.Abstractions;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    [Mapper]
    public partial class EmployeeMapper
    {
        public partial Employee CreateEmployeeDtoToEmployee(CreateEmployeeDTO createEmployeeDTO);
        public partial Employee UpdateEmployeeToUpdateEmployeeDTO(UpdateEmployeeDTO updateEmployeeDTO);
        public partial EmployeeModelDTO EmployeeToEmployeeDTO(Employee employee);
        public partial IEnumerable<EmployeeModelDTO> EmployeeToEmployeeDTO(IEnumerable<Employee> employee);
    }
}
