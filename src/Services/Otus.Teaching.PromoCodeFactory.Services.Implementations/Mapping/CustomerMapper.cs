﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.CustomerPreference;
using Riok.Mapperly.Abstractions;
using System.Net.Http.Headers;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    [Mapper]
    public partial class CustomerMapper
    {
        [MapperIgnoreSource(nameof(CreateCustomerDTO.PreferencesIds))]
        public partial Customer CreateCustomerDtoToCustomer(CreateCustomerDTO createCustomerDTO);

        [MapperIgnoreSource(nameof(CreateCustomerDTO.PreferencesIds))]
        public partial Customer CreateCustomerDtoToCustomer(UpdateCustomerDTO createCustomerDTO);

        public Customer ConvertCreateCustomerDtoToCustomer(CreateCustomerDTO createCustomerDTO)
        {
            var customer = CreateCustomerDtoToCustomer(createCustomerDTO);
            if(createCustomerDTO.PreferencesIds != null && createCustomerDTO.PreferencesIds.Count > 0)
            {
                var prefecrences = new List<CustomerPreference>();
                foreach(var preference in createCustomerDTO.PreferencesIds)
                {
                    prefecrences.Add(new CustomerPreference()
                    {
                        Customer = customer,
                        PreferenceId = preference
                    });
                }
                customer.CustomerPreferences = prefecrences;
            }
            return customer;
        }

        public Customer ConvertUpdateCustomerDtoToCustomer(UpdateCustomerDTO createCustomerDTO)
        {
            var customer = CreateCustomerDtoToCustomer(createCustomerDTO);
            var prefecrences = new List<CustomerPreference>();
            if (createCustomerDTO.PreferencesIds != null && createCustomerDTO.PreferencesIds.Count > 0)
            {
                foreach (var preference in createCustomerDTO.PreferencesIds)
                {
                    prefecrences.Add(new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        PreferenceId = preference
                    });
                }
                customer.CustomerPreferences = prefecrences;
            }
            customer.CustomerPreferences = prefecrences;
            return customer;
        }

        [MapperIgnoreSource(nameof(Customer.Promocodes))]
        [MapperIgnoreSource(nameof(Customer.CustomerPreferences))]
        public partial CustomerModelDTO CustomerToCustomerModelDto(Customer customer);

        public partial List<CustomerModelDTO> CustomerToCustomerModelDto(List<Customer> customer);

        public CustomerModelDTO ConvertCustomerToCustomerModelDto(Customer customer)
        {
            var customerDto = CustomerToCustomerModelDto(customer);
            if(customer.CustomerPreferences != null && customer.CustomerPreferences.Count > 0)
            {
                var customerPreferences = new List<CustomerPreferenceDTO>();
                foreach(var preference in customer.CustomerPreferences)
                {
                    customerPreferences.Add(new CustomerPreferenceDTO()
                    {
                        CustomerId = preference.CustomerId,
                        PreferenceId = preference.PreferenceId,
                        Preference = new Contracts.Preference.PreferenceModelDTO()
                        {
                            Id = preference.Preference.Id,
                            Name = preference.Preference.Name,
                        },
                    });
                }
                customerDto.CustomerPreferences = customerPreferences;
            }
            return customerDto;
        }
        public List<CustomerModelDTO> ConvertCustomerToCustomerModelDto(List<Customer> customer)
        {
            return customer.Select(x => ConvertCustomerToCustomerModelDto(x)).ToList();
        }
    }
}
