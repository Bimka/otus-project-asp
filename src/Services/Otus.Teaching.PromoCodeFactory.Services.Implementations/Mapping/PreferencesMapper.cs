﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Preference;
using Riok.Mapperly.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    [Mapper]
    public partial class PreferencesMapper
    {
        public partial PreferenceModelDTO PreferenceToPreferenceModelDto(Preference preference);
        public partial List<PreferenceModelDTO> PreferenceToPreferenceModelDto(List<Preference> preferences);
    }
}
