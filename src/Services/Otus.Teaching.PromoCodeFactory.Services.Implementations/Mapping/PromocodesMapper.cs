﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Promocode;
using Riok.Mapperly.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    [Mapper]
    public partial class PromocodesMapper
    {
        public partial PromocodeModelDTO PromoCodeModelDtoToPromoCodeShortResponse(Promocode promocode);
        public partial List<PromocodeModelDTO> PromoCodeModelDtoToPromoCodeShortResponse(List<Promocode> promocode);
    }
}
