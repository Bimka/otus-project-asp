﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Promocode;
using Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class PromocodesService(IPromocodesRepository repository) : IPromocodesService
    {
        private readonly IPromocodesRepository _repository = repository;
        private readonly PromocodesMapper _mapper = new();
        public async Task<List<PromocodeModelDTO>> GetAllAsync()
        {
            return _mapper.PromoCodeModelDtoToPromoCodeShortResponse(await _repository.GetAllAsync(CancellationToken.None));
        }
    }
}
