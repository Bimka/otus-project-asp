﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Employee;
using Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class EmployeeService(IEmployeeRepository employeeRepository) : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository = employeeRepository;
        private readonly EmployeeMapper _employeeMapper = new();
        public async Task CreateAsync(CreateEmployeeDTO createEmployeeDTO)
        {
            await _employeeRepository.AddAsync(_employeeMapper.CreateEmployeeDtoToEmployee(createEmployeeDTO));
            await _employeeRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            _employeeRepository.Delete(id);
            await _employeeRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<EmployeeModelDTO>> GetAllAsync()
        {
            return _employeeMapper.EmployeeToEmployeeDTO(await _employeeRepository.GetAllAsync(CancellationToken.None));
        }

        public async Task<EmployeeModelDTO> GetAsync(int id)
        {
            return _employeeMapper.EmployeeToEmployeeDTO(await _employeeRepository.GetAsync(id, CancellationToken.None));
        }

        public Task UpdateAsync(UpdateEmployeeDTO updateEmployeeDTO)
        {
            throw new NotImplementedException();
        }
    }
}
