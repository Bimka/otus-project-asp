﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Preference;
using Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class PreferencesService(IPreferencesRepository preferencesRepository) : IPreferencesService
    {
        private readonly IPreferencesRepository _preferencesRepository = preferencesRepository;
        private readonly PreferencesMapper _preferencesMapper = new ();

        public async Task<List<PreferenceModelDTO>> GetAllAsync()
        {
            return _preferencesMapper.PreferenceToPreferenceModelDto(await _preferencesRepository.GetAllAsync(CancellationToken.None));
        }
    }
}
