﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Preference;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions
{
    public interface IPreferencesService
    {
        Task<List<PreferenceModelDTO>> GetAllAsync();
    }
}
