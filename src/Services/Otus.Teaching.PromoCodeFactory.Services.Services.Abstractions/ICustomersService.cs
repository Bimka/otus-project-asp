﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions
{
    public interface ICustomersService
    {
        public Task<int> CreateCustomerAsync(CreateCustomerDTO customer);
        Task DeleteCustomerAsync(int id);
        Task EditCustomersAsync(UpdateCustomerDTO customer);
        public Task<List<CustomerModelDTO>> GetAllAsync();
        public Task<CustomerModelDTO> GetByIdAsync(int id);
    }
}
