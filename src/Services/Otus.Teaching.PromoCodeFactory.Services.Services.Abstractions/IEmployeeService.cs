﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Employee;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions
{
    public interface IEmployeeService
    {
        Task CreateAsync(CreateEmployeeDTO createEmployeeDTO);
        Task DeleteAsync(int id);
        Task<IEnumerable<EmployeeModelDTO>> GetAllAsync();
        Task<EmployeeModelDTO> GetAsync(int id);
        Task UpdateAsync(UpdateEmployeeDTO updateEmployeeDTO);
    }
}
