﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts.Promocode;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions
{
    public interface IPromocodesService
    {
        Task<List<PromocodeModelDTO>> GetAllAsync();
    }
}
