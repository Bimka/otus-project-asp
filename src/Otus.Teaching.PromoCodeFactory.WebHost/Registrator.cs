﻿using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class Registrator
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings)
                    .AddSingleton((IConfigurationRoot)configuration)
                                        .InstallServices()
                    .ConfigureContext(applicationSettings.ConnectionString)
                    .InstallRepositories()
                    .AddControllers(options => options.SuppressAsyncSuffixInActionNames = false);
            return services;
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddScoped<IEmployeeService, EmployeeService>()
                .AddScoped<ICustomersService, CustomersService>()
                .AddScoped<IPreferencesService, PreferencesService>()
                .AddScoped<IPromocodesService, PromocodesService>()
                .AddScoped<IDbInitializer, EfDbInitializer>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddScoped<IEmployeeRepository, EmployeeRepository>()
                .AddScoped<ICustomersRepository, CustomersRepository>()
                .AddScoped<IPreferencesRepository, PreferencesRepository>()
                .AddScoped<IPromocodesRepository, PromocodesRepository>();
            return serviceCollection;
        }
    }
}
