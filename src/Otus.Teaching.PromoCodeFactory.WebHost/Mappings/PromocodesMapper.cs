﻿using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.PromoCode;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Promocode;
using Riok.Mapperly.Abstractions;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    [Mapper]
    public partial class PromocodesMapper
    {
        public partial PromocodeShortResponse PromoCodeModelDtoToPromoCodeShortResponse(PromocodeModelDTO promoCodeModelDTO);
        public partial List<PromocodeShortResponse> PromoCodeModelDtoToPromoCodeShortResponse(List<PromocodeModelDTO> promoCodeModelDTO);
    }
}
