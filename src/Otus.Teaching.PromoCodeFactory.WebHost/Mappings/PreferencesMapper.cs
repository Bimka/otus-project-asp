﻿using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Preference;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Preference;
using Riok.Mapperly.Abstractions;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    [Mapper]
    public partial class PreferencesMapper
    {
        public partial PreferenceResponse PreferenceModelDtoToPreferenceModel(PreferenceModelDTO preferenceModelDTO);
        public partial IEnumerable<PreferenceResponse> PreferenceModelDtoToPreferenceModel(IEnumerable<PreferenceModelDTO> preferenceModels);
    }
}
