﻿using Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Customer;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Customers;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Preference;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Customer;
using Riok.Mapperly.Abstractions;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    [Mapper]
    public partial class CustomerMapper
    {
        public partial CustomerResponse CustomerModelDtoToCustomerResponse(CustomerModelDTO customerModelDTO);
        public partial CustomerResponse CustomerModelDtoToCustomerShortResponse(CustomerModelDTO customerModelDTO);
        public partial IEnumerable<CustomerResponse> CustomerModelDtoToCustomerShortResponse(IEnumerable<CustomerModelDTO> customerModelDTO);
        public partial CreateCustomerDTO CreateCustomerRequestToCreateCustomerDto(CreateCustomerRequest createCustomerRequest);
        public partial UpdateCustomerDTO UpdateCustomerRequestToUpdateCustomerDto(UpdateCustomerRequest updateCustomerRequest);

        public CustomerResponse ConvertCustomerModelDtoToCustomerResponse(CustomerModelDTO customerModelDTO)
        {
            var customer = CustomerModelDtoToCustomerResponse(customerModelDTO);
            if(customerModelDTO.CustomerPreferences != null && customerModelDTO.CustomerPreferences.Count > 0)
            {
                var prefernces = new List<PreferenceResponse>();
                foreach(var preference in customerModelDTO.CustomerPreferences)
                {
                    prefernces.Add(new PreferenceResponse()
                    {
                        Id = preference.Preference.Id,
                        Name = preference.Preference.Name,
                    });
                }
                customer.Preferences = prefernces;
            }
            return customer;
        }
    }
}
