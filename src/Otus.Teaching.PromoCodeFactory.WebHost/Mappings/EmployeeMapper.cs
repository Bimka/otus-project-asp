﻿using Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Empoloyee;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Employee;
using Otus.Teaching.PromoCodeFactory.Services.Contracts.Employee;
using Riok.Mapperly.Abstractions;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    [Mapper]
    public partial class EmployeeMapper
    {
        public partial CreateEmployeeDTO CreateEmployeeToCreateEmployeeDto(CreateEmployeeRequest createEmployeeRequest);
        public partial EmployeeModel EmployeeModelDTOToEmployeeModel(EmployeeModelDTO employeModelDTO);
        public partial UpdateEmployeeDTO UpdateEmployeeToUpdateEmployeeDTO(UpdateEmployeeRequest updateEmployeeDTO);
        public partial IEnumerable<EmployeeModel> EmployeeModelDTOToEmployeModel(IEnumerable<EmployeeModelDTO> employeModelDTO);
    }
}
