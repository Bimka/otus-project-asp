﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappings;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController(IPreferencesService preferencesService) : ControllerBase
    {
        private readonly IPreferencesService _preferencesService = preferencesService;
        private readonly PreferencesMapper _preferencesMapper = new ();

        [HttpGet]
        public async Task<ActionResult> GetPreferencesAsync()
        {
            return Ok(_preferencesMapper.PreferenceModelDtoToPreferenceModel(await _preferencesService.GetAllAsync()));
        }
    }
}
