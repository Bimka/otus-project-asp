﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappings;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Promocode;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromocodesController(IPromocodesService promocodesService) : ControllerBase
    {
        private readonly IPromocodesService _promocodesService = promocodesService;
        private readonly PromocodesMapper _promocodesMapper = new ();

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetPromocodesAsync()
        {
            return Ok(_promocodesMapper.PromoCodeModelDtoToPromoCodeShortResponse(await _promocodesService.GetAllAsync()));
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            throw new NotImplementedException();
        }
    }
}
