﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Customer;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappings;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController(ICustomersService customersService) : ControllerBase
    {
        private readonly ICustomersService _customersService = customersService;
        private readonly CustomerMapper _customerMapper = new();

        [HttpGet]
        public async Task<ActionResult> GetCustomersAsync()
        {
            return Ok(_customerMapper.CustomerModelDtoToCustomerShortResponse(await _customersService.GetAllAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetCustomerByIdAsync(int id)
        {
            return Ok(_customerMapper.ConvertCustomerModelDtoToCustomerResponse(await _customersService.GetByIdAsync(id)));
        }

        [HttpPost]
        public async Task<ActionResult> CreateCustomerAsync(CreateCustomerRequest createCustomerRequest)
        {
            var customer = _customerMapper.CreateCustomerRequestToCreateCustomerDto(createCustomerRequest);
            var customerId = await _customersService.CreateCustomerAsync(customer);
            return CreatedAtAction(nameof(GetCustomerByIdAsync), new { id = customerId }, null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(int id, UpdateCustomerRequest updateCustomerRequest)
        {
            var customer = await _customersService.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customersService.EditCustomersAsync(_customerMapper.UpdateCustomerRequestToUpdateCustomerDto(updateCustomerRequest));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomerAsync(int id)
        {
            var customer = await _customersService.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customersService.DeleteCustomerAsync(id);

            return NoContent();
        }
    }
}
