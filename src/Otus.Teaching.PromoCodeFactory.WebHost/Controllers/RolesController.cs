﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Responses.Role;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController(IRepository<Role, short> rolesRepository)
    {
        private readonly IRepository<Role, short> _rolesRepository = rolesRepository;

        #region CREATE

        #endregion

        #region READ

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //public async Task<List<RoleModel>> GetRolesAsync()
        //{
        //    var roles = await _rolesRepository.GetAllAsync();

        //    var rolesModelList = roles.Select(x => new RoleModel()
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        Description = x.Description
        //    }).ToList();

        //    return rolesModelList;
        //}

        #endregion

        #region UPDATE

        #endregion

        #region DELETE

        #endregion
    }
}