﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Services.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.ModelsAPI.Requests.Empoloyee;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappings;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController(IEmployeeService employeeService) : ControllerBase
    {
        private readonly IEmployeeService _employeeService = employeeService;
        private readonly EmployeeMapper _mapper = new();

        #region CREATE

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(CreateEmployeeRequest employee)
        {
            await _employeeService.CreateAsync(_mapper.CreateEmployeeToCreateEmployeeDto(employee));
            return Ok();
        }

        #endregion

        #region READ

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetEmployeesAsync()
        {
            var employees = await _employeeService.GetAllAsync();
            return Ok(_mapper.EmployeeModelDTOToEmployeModel(employees));
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployeeByIdAsync(int id)
        {
            var employee = await _employeeService.GetAsync(id);
            if (employee == null) return NotFound();

            return Ok(_mapper.EmployeeModelDTOToEmployeeModel(employee));
        }

        #endregion

        #region UPDATE

        /// <summary>
        /// Обновить информацию по сотруднику
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(UpdateEmployeeRequest employee)
        {
            await _employeeService.UpdateAsync(_mapper.UpdateEmployeeToUpdateEmployeeDTO(employee));
            return Ok();
        }

        #endregion

        #region DELETE

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployeeAsync(int id)
        {
            await _employeeService.DeleteAsync(id);
            return Ok();
        }

        #endregion
    }
}