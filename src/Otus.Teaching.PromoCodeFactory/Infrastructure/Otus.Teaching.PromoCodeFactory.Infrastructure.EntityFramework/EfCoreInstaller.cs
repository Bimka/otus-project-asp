﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework
{
    public static class EfCoreInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<OtusPromoCodeFactoryContext>(optionsBuilder
                => optionsBuilder.UseSqlite(connectionString));

            return services;
        }

        
    }
}
