﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Intarfaces
{
    public interface IEntity<TPrimaryKey>
    {
        TPrimaryKey Id { get; set; }
    }
}
