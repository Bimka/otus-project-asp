﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;
using Otus.Teaching.PromoCodeFactory.Core.Intarfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;

public class Preference : IEntity<short>
{
    public short Id { get; set; }

    public string Name { get; set; }

    public ICollection<CustomerPreference> CustomerPreferences { get; set; } 

    public ICollection<Promocode> Promocodes { get; set; }

}