﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;
using Otus.Teaching.PromoCodeFactory.Core.Intarfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;

public class Role : IEntity<short>
{
    public short Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public ICollection<Employee> Employees { get; set; } = new List<Employee>();
}