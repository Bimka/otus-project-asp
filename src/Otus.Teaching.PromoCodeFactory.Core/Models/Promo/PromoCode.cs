﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Core.Intarfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;

public class Promocode : IEntity<int>
{
    public int Id { get; set; }

    public int EmployeeId { get; set; }

    public int CustomerId { get; set; }

    public short PreferenceId { get; set; }

    public virtual Customer Customer { get; set; }

    public virtual Employee Employee { get; set; }

    public virtual Preference Preference { get; set; }
}