﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;
using Otus.Teaching.PromoCodeFactory.Core.Intarfaces;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Info;

public class Customer : IEntity<int>
{
    public int Id { get; set; }

    public string LastName { get; set; }

    public string FirstName { get; set; }

    public string MiddleName { get; set; }

    public ICollection<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();

    public ICollection<Promocode> Promocodes { get; set; } = new List<Promocode>();

}