﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Core.Intarfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Info;

public class CustomerPreference : IEntity<int>
{
    public int Id { get; set; }

    public int CustomerId { get; set; }

    public short PreferenceId { get; set; }

    public Customer Customer { get; set; }

    public Preference Preference { get; set; }
}