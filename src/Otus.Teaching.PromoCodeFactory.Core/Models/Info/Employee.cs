﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;
using Otus.Teaching.PromoCodeFactory.Core.Intarfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Info;

public class Employee : IEntity<int>
{
    public int Id { get; set; }

    public string LastName { get; set; }

    public string FirstName { get; set; }

    public string MiddleName { get; set; }

    public short RoleId { get; set; }

    public string Email { get; set; }

    public ICollection<Promocode> Promocodes { get; set; } = new List<Promocode>();

    public Role Role { get; set; }
}