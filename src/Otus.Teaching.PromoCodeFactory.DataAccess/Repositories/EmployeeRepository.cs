﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository(OtusPromoCodeFactoryContext context) : Repository<Employee, int>(context), IEmployeeRepository
    {
    }
}
