﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferencesRepository(OtusPromoCodeFactoryContext context) : Repository<Preference, short>(context), IPreferencesRepository
    {
    }
}
