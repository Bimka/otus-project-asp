﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Promo;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromocodesRepository(OtusPromoCodeFactoryContext context) : Repository<Promocode, int>(context), IPromocodesRepository
    {
    }
}
