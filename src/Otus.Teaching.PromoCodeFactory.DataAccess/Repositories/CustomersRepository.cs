﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomersRepository(OtusPromoCodeFactoryContext context) : Repository<Customer, int>(context), ICustomersRepository
    {
        /// <summary>
        /// Запросить все сущности в базе.
        /// </summary>
        public new async Task<List<Customer>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false)
        {
            var query = GetAll();
            return await query
                .Include(x => x.CustomerPreferences)
                    .ThenInclude(x => x.Preference)
                .Include(x => x.Promocodes).ToListAsync();
        }

        /// <summary>
        /// Получить сущность по Id.
        /// </summary>
        public new async Task<Customer> GetAsync(int id, CancellationToken cancellationToken)
        {
            return await GetAll().Where(x => x.Id == id)
                .Include(x => x.CustomerPreferences)
                    .ThenInclude(x => x.Preference)
                .Include(x => x.Promocodes)
                .FirstOrDefaultAsync();
        }

        public new async Task Delete(int id)
        {
            var customerToRemove = await GetAll().Where(x => x.Id == id)
                .Include(x => x.CustomerPreferences)
                .Include(x => x.Promocodes)
                .FirstOrDefaultAsync();

            Context.Remove(customerToRemove);
        }
    }
}
