﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Handbooks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Info;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Employee> Employees =>
        [
            new()
            {
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                MiddleName ="Сергеевич",
                RoleId = 1,
            },
            new()
            {
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                MiddleName ="Сергеевич",
                RoleId = 2,
                
            },
        ];

        public static List<Role> Roles =>
        [
            new()
            {
                Name = "Admin",
                Description = "Администратор",
            },
            new()
            {
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        ];


        public static List<Preference> Preferences =>
        [
            new ()
            {
                Name = "Театр",
            },
            new ()
            {
                Name = "Семья",
            },
            new ()
            {
                Name = "Дети",
            }
        ];

        public static List<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new ()
                    {
                        FirstName = "Иван",
                        LastName = "Петров",
                        MiddleName ="Петрович",
                        CustomerPreferences =
                        [
                            new CustomerPreference()
                            {
                                PreferenceId = 1
                            },
                            new CustomerPreference()
                            {
                                PreferenceId = 2
                            }
                        ]
                    }
                };

                return customers;
            }
        }
    }
}