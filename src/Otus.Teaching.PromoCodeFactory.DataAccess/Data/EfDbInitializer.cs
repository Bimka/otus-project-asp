﻿using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer(OtusPromoCodeFactoryContext dataContext) : IDbInitializer
    {
        private readonly OtusPromoCodeFactoryContext _dataContext = dataContext;

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Roles);
            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();
        }
    }
}
